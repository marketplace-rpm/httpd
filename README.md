# Information / Информация

SPEC-файл для создания RPM-пакета **httpd**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/httpd`.
2. Установить пакет: `dnf install httpd`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)